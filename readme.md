# Crowndsale on Ethereum.

Crowdsale est un smartcontract pour une levée de fond.
Ici on prend EURD qui est fixé à 369€ pour 1ETH

## Installation
Installation des dépendances nodeJS 

```bash
npm install
```

## Usage
```bash
truffle test
```

## License
[MIT](https://choosealicense.com/licenses/mit/)