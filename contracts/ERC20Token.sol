// Crowdsale.sol
// SPDX-License-Identifier: MIT
pragma solidity 0.6.11;


/**
 * Implementaiton de l'ERC20 d'OpenZeppelin
 * */

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract ERC20Token is ERC20{  

   constructor(uint256 initialSupply) public ERC20("EURO DIGITAL", "EURD") {
       //Create totelSupply et l'affecte à msg.sender 
       _mint(msg.sender, initialSupply);
   }
}
