// SPDX-License-Identifier: MIT
pragma solidity 0.6.11;

import "./ERC20Token.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/math/SafeMath.sol";
import "@openzeppelin/contracts/utils/ReentrancyGuard.sol";

/**
 * Crowdsale example 
 * TODO withdraw
 * TODO payment to stableCoin
 * TODO presale et sale phase 
 */
contract Crowdsale is Ownable, ReentrancyGuard {
    using SafeMath for uint256;

    /**
     * contains address to withdraw
     */
    struct WithdrawAddress {
        address addressToWithdraw;
        uint amount;
    }

    /**
     * FIXME et les centimes ?
     * Rate is 1 ETH is 379 EUR
     * So 1ETH = 10^18 / 379
     */
    uint256 public rate = 2638522427440633; // the rate to use

    ERC20Token public token;

    mapping(address=>uint) private mapWithdraw;

    WithdrawAddress[] withdrawAddressList;

    event EventReceived(address addressSender, uint256 amount);

    event EventDeposited(address addressReceive, uint256 amount);

    event EventWithdraw(address addressReceive, uint256 amount);

    uint public mincap=1000;

    //constructor(uint256 initialSupply, uint start_presale, uint start_sale, uint end_sale, uint end_refund) public {
    constructor(uint256 initialSupply) public {
        //c'est le contrat qui instancie donc c'est lui le owner;
        token = new ERC20Token(initialSupply);
    }

    /**
     * Obligatoire pour recevoir de l'ether depuis la V0.5.0
     * */
    receive() external payable {
        require(msg.value >= 0.1 ether, "you can't sent less than 0.1 ether");
        emit EventReceived(msg.sender, msg.value);
        // deprecate avec pull payyment withdrawAddressList.push(WithdrawAddress(msg.sender, msg.value));
        mapWithdraw[msg.sender]=msg.value;
        distribute(msg.sender,msg.value);
    }

    /**
     * Fonction de distribution des le token en fonct des ETH
     * DEPRECATED en internal, msg.sender est le contrat !
     * */
    // function distribute(uint256 amount) internal {
    //     uint256 tokensToSent = amount / rate;
    //     token.transfer(msg.sender, tokensToSent);
    //     emit EventDeposited(msg.sender, tokensToSent);
    // }

    /**
     * FIXME verifier le deprecated du dessus ! (en fait avant je transferai sur le contrat !!!)
     * Fonction de distribution des le token en fonct des ETH
     * DEPRECATED en internal, msg.sender est le contrat !
     * */
    function distribute(address recipient, uint256 amount) internal {
        uint256 tokensToSent = amount / rate;
        token.transfer(recipient, tokensToSent);
        emit EventDeposited(recipient, tokensToSent);
    }

    /**
     * deprecated
     * FIXME deprecated because pb of gaz limit choice PULL PAYMENT https://fravoll.github.io/solidity-patterns/pull_over_push.html
     * dev : if there are some pb, we need widthraw all people
     */
    // function widthrawAll() public nonReentrant onlyOwner {
    //     //FIXME attention à la limite de gaz par bloc
    //     require(withdrawAddressList.length!=0, "No withdraw to do");
    //     uint i;
    //     for(i=0; i<withdrawAddressList.length; i++){
    //         if(withdrawAddressList[i].amount!=0){
    //             withdrawAddressList[i].addressToWithdraw.call{value : withdrawAddressList[i].amount, gas: 3300}("");
    //             emit EventWithdraw(withdrawAddressList[i].addressToWithdraw,withdrawAddressList[i].amount);
    //             withdrawAddressList[i].amount=0; 
    //         }
    //     }
    // }

    function refund() public {
        //require(address(this).balance < mincap, "mincap non atteint");//refund seulement di mincap pas atteint
        require(mapWithdraw[msg.sender] > 0);//est ce que l'on a deposé de l'ether
        // require(block.timestamp<=end_sale);
        msg.sender.transfer(mapWithdraw[msg.sender]);
    }
}
