//interface avec le contrat
const Crowdsale = artifacts.require("Crowdsale");
const MyContractToken = artifacts.require('ERC20Token');//pour avoir les infos stocké dans heritage ERC20
const ERC20TokenABI = MyContractToken.toJSON().abi;

// const { BN } = require('@openzeppelin/test-helpers');
// const { expect } = require('chai');

contract("Crowdsale", (accounts) => {
    //liaison avec les comptes de gannache
    let [creator, user1, user2] = accounts;

    const totalSupply = 300000;

    // beforeEach(async function () {
    //     // this.ERC20Instance = await ERC20.new(_initialsupply);
    //     // await this.ERC20Instance.transfer(account1, amount);
    //     this.crowdsaleInstance=await Crowdsale.new(totalSupply);

    //     let addressContractToken = await this.crowdsaleInstance.token.call();
    //     this.tokenInstance = await MyContractToken.at(addressContractToken);
    // });

    //test
    it("Je teste le deploiement du contrat et un achat de tocken", async () => {
        //creation de l'instance du contrat.
        let totalSupply = 300000;
        let amountEth = web3.utils.toWei('1', "ether");
        let amountEurd = 379;


        let crowdsaleInstance = await Crowdsale.new(totalSupply);
        let addressContractToken = await crowdsaleInstance.token.call();
        let tokenInstance = await MyContractToken.at(addressContractToken);


        //je teste le total supply 
        let resultTotalSupply = await tokenInstance.totalSupply.call();
        console.log("resultTotalSupply" + resultTotalSupply);
        assert.equal(resultTotalSupply, totalSupply, "Total supply trouble!");

        /*
        * Je teste le depot de 1 ether contre 379€
        */

        //On teste la balance avant l'achat de l'EURD
        let amountUser1Before = await web3.utils.fromWei(await web3.eth.getBalance(user1), 'ether');
        console.log("TEST balance de user1:" + amountUser1Before);


        /*
        * Je teste le transfert
        */
        result = await crowdsaleInstance.sendTransaction({ from: user1, to: crowdsaleInstance.address, value: amountEth, gas:0 });
        //web3.eth.sendTransaction({from: accounts[1], to: instance.address, value: one_eth});
        assert.equal(result.receipt.status, true, "Result to set must be true !");
        let eventReceived = result.logs[0];
        assert.equal(eventReceived.args.addressSender, user1, "Sender must be=" + user1);
        assert.equal(eventReceived.args.amount, amountEth, "Expected amount=" + amountEth);


        /*
        * Je teste que je recupere les EURD
        */
        let eventDeposited = result.logs[1];
        assert.equal(eventDeposited.args.addressReceive, user1, "Sender must be=" + user1);
        assert.equal(eventDeposited.args.amount, amountEurd, "Expected amount=" + amountEurd);

        //FIXME comment faire l'appel à une vraiable
        // let addressContractToken=await CrowdsaleInstance.token.call();

        /*
        * Je teste que la balance de l'utilisateur est debité 
        */
        let amountUser1After = await web3.utils.fromWei(await web3.eth.getBalance(user1), 'ether');
        console.log("TEST balance de user1 apres:" + amountUser1After);
        assert.equal(Math.trunc(amountUser1Before - 1), Math.trunc(amountUser1After), "test de la balance user1");

        //remboursement
        await crowdsaleInstance.refund({ from: user1 });

    })

    it("Je teste un remboursement", async () => {

        //creation de l'instance du contrat.
        let totalSupply = 300000;
        let amountEth = web3.utils.toWei('1', "ether");
        let amountEurd = 379;

        let crowdsaleInstance = await Crowdsale.new(totalSupply);

        //convert eth to wei
        //let nbWei=web3.utils.toWei('1',"ether");

        // const CrowdsaleInstance = await Crowdsale.new(totalSupply);
        /*
        * Je teste le depot de 1 ether contre 379€
        */
        //On teste la balance avant l'achat de l'EURD
        let amountUser1Before = await web3.utils.fromWei(await web3.eth.getBalance(user1), 'ether');
        console.log("TEST balance de user1:" + amountUser1Before);


        /*
        * Je teste le transfert
        */
        result = await crowdsaleInstance.sendTransaction({ from: user1, to: crowdsaleInstance.address, value: amountEth, gas:0  });
        //web3.eth.sendTransaction({from: accounts[1], to: instance.address, value: one_eth});
        assert.equal(result.receipt.status, true, "Result to set must be true !");
        let eventReceived = result.logs[0];
        assert.equal(eventReceived.args.addressSender, user1, "Sender must be=" + user1);
        assert.equal(eventReceived.args.amount, amountEth, "Expected amount=" + amountEth);

        /*
        * Je teste que je recupere les EURD
        */
        let eventDeposited = result.logs[1];
        assert.equal(eventDeposited.args.addressReceive, user1, "Sender must be=" + user1);
        assert.equal(eventDeposited.args.amount, amountEurd, "Expected amount=" + amountEurd);

        /*
        * Je teste que la balance de l'utilisateur est debité 
        */
        let amountUser1After = await web3.utils.fromWei(await web3.eth.getBalance(user1), 'ether');
        console.log("TEST balance de user1 apres:" + amountUser1After);
        assert.equal(Math.trunc(amountUser1Before - 1), Math.trunc(amountUser1After), "test de la balance user1");

        //je teste le remboursement
        result = await crowdsaleInstance.refund({ from: user1 });
        assert.equal(result.receipt.status, true, "Result to set must be true !");

        let amountUser1AfterWithdraw = await web3.utils.fromWei(await web3.eth.getBalance(user1), 'ether');
        console.log("TEST balance de user1 apres withdraw:" + amountUser1AfterWithdraw);
        assert.equal(Math.trunc(amountUser1Before), Math.trunc(amountUser1AfterWithdraw), "test de la balance user1");
    })

})